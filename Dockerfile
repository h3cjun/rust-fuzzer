FROM rustlang/rust:nightly

RUN apt-get update -qq && apt-get install -y -qq git make clang cmake
RUN export CC=`which clang`
RUN export CXX=`which clang++` 
RUN cargo install cargo-fuzz
